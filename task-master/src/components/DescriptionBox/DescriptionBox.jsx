import React, { useState } from "react";
import gsap, { Power3 } from "gsap";
import PropTypes from "prop-types";
import ArrowRightRoundedIcon from "@material-ui/icons/ArrowRightRounded";
import s from "./descriptionBox.module.scss";

const DiscriptionBox = ({ obj, arrowVisible }) => {
  const [opened, setOpened] = useState(false);

  let descriptionProps = null;
  let backgroundProps = null;
  let arrowProps = null;
  const setOpenDiscription = () => {
    setOpened(!opened);
    gsap.to(descriptionProps, {
      duration: 0.5,
      height: opened ? 0 : backgroundProps.clientHeight,
      ease: Power3.easeOut,
    });
    gsap.to(arrowProps, {
      duration: 0.5,
      transform: opened ? "rotate(0deg)" : "rotate(90deg)",
      ease: Power3.easeOut,
    });
  };

  return (
    <div>
      <button
        type="button"
        className={s.arrowButton}
        style={{
          visibility: arrowVisible ? "visible" : "hidden",
        }}
        onClick={setOpenDiscription}
        // eslint-disable-next-line no-return-assign
        ref={(e) => (arrowProps = e)}
      >
        <ArrowRightRoundedIcon />
      </button>
      <div
        className={s.taskDescription}
        // eslint-disable-next-line no-return-assign
        ref={(e) => (descriptionProps = e)}
      >
        <div
          className={s.taskDescription__background}
          // eslint-disable-next-line no-return-assign
          ref={(e) => (backgroundProps = e)}
        >
          <p>{obj.description}</p>
        </div>
      </div>
    </div>
  );
};

DiscriptionBox.defaultProps = {
  arrowVisible: true,
};

DiscriptionBox.propTypes = {
  obj: PropTypes.object.isRequired,
  arrowVisible: PropTypes.bool,
};

export default DiscriptionBox;
