import React from "react";
import PropTypes from "prop-types";
import s from "./shopCard.module.scss";

const ShopCard = ({ obj, bought, points }) => {
  const shopItem = (item) => {
    return (
      <div className={s.bottom__main}>
        <div className={s.bottom__main__title}>
          <h3>{item.title}</h3>
        </div>
        <div className={s.bottom__main__cost}>
          <p>{`${item.cost}pts`}</p>
        </div>
        <div className={s.bottom__main__button}>
          <button
            type="button"
            onClick={() => bought(item)}
            style={{
              color: points >= item.cost ? "green" : "red",
            }}
          >
            Buy
          </button>
        </div>
      </div>
    );
  };

  return (
    <div className={s.root}>
      <div className={s.top}>
        <div className={s.top__title}>
          <h3>{obj.title}</h3>
        </div>
        <div className={s.top__user}>
          <h3>Melanie Backens</h3>
          <div className={s.top__user__avatar}>MB</div>
        </div>
      </div>
      <div className={s.bottom}>{obj.items.map((item) => shopItem(item))}</div>
    </div>
  );
};

ShopCard.defaultProps = {
  bought: "",
  points: Infinity,
};

ShopCard.propTypes = {
  obj: PropTypes.array.isRequired,
  bought: PropTypes.func,
  points: PropTypes.number,
};

export default ShopCard;
