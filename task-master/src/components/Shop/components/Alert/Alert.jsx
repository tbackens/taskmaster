/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-static-element-interactions */
import React from "react";
import PropTypes from "prop-types";
import s from "./alert.module.scss";

const Alert = ({
  open,
  title,
  message,
  confirmed,
  confirmText,
  confirmColor,
  declined,
  declineText,
  declineColor,
}) => {
  return (
    <div className={s.root}>
      {open ? (
        <>
          <div className={s.shadow} onClick={declined} />
          <div className={s.alertBox}>
            <div className={s.alertBox__title}>
              <h3>{title}</h3>
            </div>
            <div className={s.alertBox__message}>
              <p>{message}</p>
            </div>
            <div className={s.alertBox__buttons}>
              <button
                type="button"
                onClick={confirmed}
                className={confirmColor ? "" : s.notUsed}
                style={{
                  backgroundColor: confirmColor,
                  color: confirmColor === "white" ? "black" : "white",
                }}
              >
                {confirmText}
              </button>
              <button
                type="button"
                onClick={declined}
                className={declineColor ? "" : s.notUsed}
                style={{
                  backgroundColor: declineColor,
                  color: declineColor === "white" ? "black" : "white",
                }}
              >
                {declineText}
              </button>
            </div>
          </div>
        </>
      ) : (
        ""
      )}
    </div>
  );
};

Alert.defaultProps = {
  title: "",
  message: "",
  confirmed: "",
  confirmText: "",
  declined: "",
  declineText: "",
  declineColor: undefined,
  confirmColor: undefined,
};

Alert.propTypes = {
  open: PropTypes.bool.isRequired,
  title: PropTypes.string,
  message: PropTypes.string,
  confirmed: PropTypes.func,
  confirmText: PropTypes.func,
  declined: PropTypes.func,
  declineText: PropTypes.func,
  declineColor: PropTypes.string,
  confirmColor: PropTypes.string,
};

export default Alert;
