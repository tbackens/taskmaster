import { v1 as uuid } from "uuid";

export default [
  {
    id: uuid(),
    title: "Computer Time",
    items: [
      {
        id: uuid(),
        title: "90 Minutes of computer time",
        cost: 700,
      },
      {
        id: uuid(),
        title: "60 Minutes of computer time",
        cost: 500,
      },
      {
        id: uuid(),
        title: "30 Minutes of computer time",
        cost: 300,
      },
      {
        id: uuid(),
        title: "2 Hours of computer time",
        cost: 900,
      },
    ],
  },
  {
    id: uuid(),
    title: "Friends",
    items: [
      {
        id: uuid(),
        title: "Go over to friend's house",
        cost: 1000,
      },
      {
        id: uuid(),
        title: "Have a friend over",
        cost: 700,
      },
      {
        id: uuid(),
        title: "Have a sleep over",
        cost: 2000,
      },
    ],
  },
];
