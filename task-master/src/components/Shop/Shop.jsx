import React, { useState } from "react";
import ShopCard from "./components/ShopCard/ShopCard";
import Alert from "./components/Alert/Alert";
import data from "./data";
import s from "./shop.module.scss";

const Shop = () => {
  const [alertOpen, setAlertOpen] = useState(false);
  const [itemProperties, setItemProperties] = useState({});

  const points = 876;

  const sendAlert = (obj) => {
    setItemProperties({
      title: obj.title,
      cost: obj.cost,
    });
    setAlertOpen(true);
  };

  const confirmed = () => {
    setAlertOpen(false);
  };
  const declined = () => {
    setAlertOpen(false);
  };

  return (
    <div className={s.root}>
      <div className={s.mainPaper}>
        <div className={s.mainPaper__top}>
          <div className={s.mainPaper__top__title}>
            <h2>Shop</h2>
          </div>
          <div className={s.mainPaper__top__points}>
            <p>{`${points}pts`}</p>
          </div>
        </div>
        <div className={s.mainPaper__bottom}>
          {data.map((obj) => (
            <ShopCard obj={obj} bought={sendAlert} points={points} />
          ))}
        </div>
      </div>
      {points >= itemProperties.cost ? (
        <Alert
          open={alertOpen}
          title="Warning"
          message={`Are you sure you want to spend ${itemProperties.cost}pts, for "${itemProperties.title}"?`}
          confirmed={confirmed}
          confirmText="Yes"
          confirmColor="#003fa3"
          declined={declined}
          declineText="No"
          declineColor="white"
        />
      ) : (
        <Alert
          open={alertOpen}
          title="Sorry"
          message={`You don't have enough points to buy "${
            itemProperties.title
          }", you are ${itemProperties.cost - points}pts short.`}
          declined={declined}
          declineText="Ok"
          declineColor="primary"
        />
      )}
    </div>
  );
};

export default Shop;
