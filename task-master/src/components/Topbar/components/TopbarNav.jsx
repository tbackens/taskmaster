/* eslint-disable no-return-assign */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-static-element-interactions */

import React, { useState } from "react";
import PropTypes from "prop-types";
import gsap from "gsap";
import Link from "../../Link";
import s from "./topbarnav.module.scss";

const TopbarNav = ({ pages, ...rest }) => {
  const [menuOpen, setMenuOpen] = useState(false);
  const menuClass = menuOpen ? s.menuOpened : "";

  let listRef = null;
  const openMenu = () => {
    gsap.to(listRef, {
      duration: 0.5,
      height: menuOpen ? 0 : "auto",
      ease: "power3.out",
    });
  };

  const updateMenu = () => {
    setMenuOpen(!menuOpen);
    openMenu();
  };

  return (
    <div className={s.root}>
      <ul {...rest} className={s.navList}>
        {pages.map((page) => (
          <li className={s.navList__item} key={page.title}>
            <Link href={page.href}>
              <div
                activeclassname={s.navList__item__active}
                className={s.navList__item__button}
              >
                <p>{page.title}</p>
              </div>
            </Link>
          </li>
        ))}
      </ul>
      <div className={`${s.menu} ${menuClass}`} onClick={updateMenu}>
        <div className={s.menu__img}>
          <svg
            width="37.5"
            height="30"
            viewBox="-5 -5 37.5 30"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
          >
            <rect
              width="25"
              height="4"
              fill="white"
              className={s.menu__img__top}
            />
            <rect
              y="8"
              width="25"
              height="4"
              fill="white"
              className={s.menu__img__mid}
            />
            <rect
              y="16"
              width="25"
              height="4"
              fill="white"
              className={s.menu__img__bot}
            />
          </svg>
        </div>
        <div className={s.menu__dropdown}>
          <ul className={s.menu__navList} ref={(e) => (listRef = e)}>
            {pages.map((page) => (
              <li className={s.menu__navList__item} key={page.title}>
                <Link href={page.href}>
                  <div className={s.menu__navList__item__button}>
                    <p>{page.title}</p>
                  </div>
                </Link>
              </li>
            ))}
          </ul>
        </div>
      </div>
    </div>
  );
};

TopbarNav.propTypes = {
  pages: PropTypes.array.isRequired,
};

export default TopbarNav;
