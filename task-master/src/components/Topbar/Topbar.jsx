import React from "react";
import TopbarNav from "./components/TopbarNav";
import Link from "../Link";
import s from "./topbar.module.scss";

const Topbar = () => {
  const pages = [
    {
      title: "Help",
      href: "/help",
    },
    {
      title: "Shop",
      href: "/shop",
    },
    {
      title: "Tasks",
      href: "/tasks",
    },
    {
      title: "My Account",
      href: "/account",
    },
  ];

  return (
    <div className={s.root}>
      <Link className={s.home} href="/">
        <img
          src="/images/TaskMaster-Logo.png"
          className={s.home__logo}
          alt="TaskMaster Logo"
        />
        <div className={s.home__button}>
          <h1 className={s.home__button__header}>TaskMaster</h1>
        </div>
      </Link>

      <TopbarNav className={s.nav} pages={pages} />
    </div>
  );
};

export default Topbar;
