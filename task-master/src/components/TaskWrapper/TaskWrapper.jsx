import React, { useState } from "react";
import gsap, { Power3 } from "gsap";
import PropTypes from "prop-types";
import SearchRoundedIcon from "@material-ui/icons/SearchRounded";
import ArrowDropDownRoundedIcon from "@material-ui/icons/ArrowDropDownRounded";
import moment from "moment";
import DescriptionBox from "../DescriptionBox/DescriptionBox";
import s from "./taskWrapper.module.scss";

const TaskWrapper = ({ id, data, taskAssignee, taskTitle }) => {
  const [tasksOpen, setTasksOpen] = useState(true);

  let assignee = "";
  let correctTasks = [];
  if (id === "Thomas Backens") {
    assignee = taskAssignee;
    correctTasks = data
      .filter((obj) => obj.assignee === assignee)
      .map((obj) => obj.tasks);
    // eslint-disable-next-line prefer-spread
    correctTasks = [].concat.apply([], correctTasks);
    correctTasks = correctTasks.filter((obj) => obj.assignedTo === id);
  } else {
    assignee = data.filter((obj) => obj.id === id).map((obj) => obj.assignee);
    [assignee] = assignee;
    correctTasks = data.filter((obj) => obj.id === id);
  }

  const initials = (nameString) =>
    nameString.split(" ")[0][0] + nameString.split(" ")[1][0];

  const renderTaskList = (obj) => {
    return (
      <>
        <div key={obj.id} className={s.mainPaper__tasksBox__taskBox}>
          <div className={s.mainPaper__tasksBox__taskBox__taskAvatar}>
            {initials(obj.assignedTo)}
          </div>
          <div className={s.mainPaper__tasksBox__taskBox__taskAssigneeName}>
            <h3>{obj.assignedTo}</h3>
          </div>
          <div className={s.mainPaper__tasksBox__taskBox__taskTitle}>
            <h3>{obj.title}</h3>
          </div>
          <div className={s.mainPaper__tasksBox__taskBox__taskInstructor}>
            {obj.completed ? (
              <p>Inspecting...</p>
            ) : (
              <div
                className={
                  s.mainPaper__tasksBox__taskBox__taskInstructor__wrapper
                }
              >
                <SearchRoundedIcon />
                <p>{assignee}</p>
              </div>
            )}
          </div>
          <div className={s.mainPaper__tasksBox__taskBox__points}>
            <p>{`${obj.points}pts`}</p>
          </div>
          <div className={s.mainPaper__tasksBox__taskBox__date}>
            <p>{moment(obj.due_date).format("MM/DD/YYYY HH:MM a")}</p>
          </div>
          <div className={s.mainPaper__tasksBox__taskBox__button}>
            {obj.completed ? (
              <p style={{ fontWeight: 500, color: "#24c724" }}>Done!</p>
            ) : (
              <button type="button">Complete</button>
            )}
          </div>
        </div>
        <DescriptionBox obj={obj} arrowVisible={tasksOpen} />
      </>
    );
  };

  let tasksProps = null;
  let arrowProps = null;
  const handleTasksOpening = () => {
    setTasksOpen(!tasksOpen);
    gsap.to(tasksProps, {
      duration: 0.5,
      height: tasksOpen ? 0 : "auto",
      ease: Power3.easeOut,
    });
    gsap.to(arrowProps, {
      duration: 0.5,
      transform: tasksOpen ? "rotate(-90deg)" : "rotate(0deg)",
      ease: Power3.easeOut,
    });
  };

  return (
    <div className={s.root}>
      <div className={s.mainPaper}>
        <div className={s.mainPaper__tasksBox}>
          <div className={s.mainPaper__tasksBox__tasksTopBar}>
            <button
              type="button"
              className={s.mainPaper__tasksBox__tasksTopBar__arrowButton}
              onClick={handleTasksOpening}
              // eslint-disable-next-line no-return-assign
              ref={(e) => (arrowProps = e)}
            >
              <ArrowDropDownRoundedIcon />
            </button>
            <div className={s.mainPaper__tasksBox__tasksTopBar__taskAvatar}>
              <div
                className={
                  s.mainPaper__tasksBox__tasksTopBar__taskAvatar__initials
                }
              >
                {initials(assignee)}
              </div>
            </div>
            <h3
              className={s.mainPaper__tasksBox__tasksTopBar__taskAssigneeName}
            >
              {assignee}
            </h3>
            <h2 className={s.mainPaper__tasksBox__tasksTopBar__tasksHeader}>
              {taskTitle}
            </h2>
          </div>
          <div
            // eslint-disable-next-line no-return-assign
            ref={(e) => (tasksProps = e)}
            style={{
              overflow: "hidden",
              backgroundColor: "rgba(0, 0, 0, 0)",
            }}
          >
            <div className={s.mainPaper__tasksBox__taskBoxSeparator} />
            <div>
              {id === "Thomas Backens"
                ? correctTasks.map((obj) => renderTaskList(obj))
                : correctTasks[0].tasks.map((obj) => renderTaskList(obj))}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

TaskWrapper.defaultProps = {
  taskAssignee: "",
  taskTitle: "Tasks",
};

TaskWrapper.propTypes = {
  id: PropTypes.string.isRequired,
  data: PropTypes.array.isRequired,
  taskAssignee: PropTypes.string,
  taskTitle: PropTypes.string,
};

export default TaskWrapper;
