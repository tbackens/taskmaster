/* eslint-disable jsx-a11y/label-has-associated-control */

import React, { useState } from "react";
import PropTypes from "prop-types";
import s from "./input.module.scss";

const Input = ({ labelText, value, setValue, Password, className }) => {
  const [active, setActive] = useState(value !== "");

  const focused = () => {
    setActive(true);
  };
  const unfocused = () => {
    if (value === "") {
      setActive(false);
    }
  };

  return (
    <div className={s.root}>
      <div className={`${s.wrapper} ${className}`}>
        <label
          className={`${s.wrapper__label} ${active ? s.active__label : ""}`}
        >
          {labelText}
        </label>
        <input
          type={Password ? "password" : ""}
          className={`${s.wrapper__input} ${active ? s.active__input : ""}`}
          value={value}
          onChange={(e) => setValue(e)}
          onFocus={focused}
          onBlur={unfocused}
        />
      </div>
    </div>
  );
};

Input.defaultProps = {
  labelText: undefined,
  value: "",
  setValue: () => {},
  Password: false,
  className: "",
};

Input.propTypes = {
  labelText: PropTypes.string,
  value: PropTypes.string,
  setValue: PropTypes.func,
  Password: PropTypes.bool,
  className: PropTypes.string,
};

export default Input;
