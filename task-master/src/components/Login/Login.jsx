/* eslint-disable jsx-a11y/label-has-associated-control */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-static-element-interactions */

import React, { useState } from "react";
import PropTypes from "prop-types";
import s from "./login.module.scss";
import Input from "./components/Input/Input";

const Login = ({ open, confirmText, declineText, onConfirm, onDecline }) => {
  const [modalOpen, setModalOpen] = useState(open);

  const [accountInfo, setAccountInfo] = useState({
    firstName: "",
    lastName: "",
    username: "",
    password: "",
    confirmPassword: "",
  });

  const updateAccountInfo = (e, name) => {
    setAccountInfo({ ...accountInfo, [name]: e.target.value });
  };

  const closeModal = () => {
    setModalOpen(false);
  };

  const confirmed = () => {
    onConfirm();
  };
  const declined = () => {
    onDecline();
    closeModal();
  };

  return (
    <div className={s.root}>
      {modalOpen ? (
        <>
          <div className={s.shadow} onClick={closeModal} />
          <div className={s.modal}>
            <h1>Sign Up</h1>

            <div className={s.modal__inputs}>
              <div className={s.modal__inputs__names}>
                <Input
                  labelText="First name"
                  value={accountInfo.firstName}
                  setValue={(e) => updateAccountInfo(e, "firstName")}
                  className={s.modal__inputs__names__first}
                />
                <Input
                  labelText="Last name"
                  value={accountInfo.lastName}
                  setValue={(e) => updateAccountInfo(e, "lastName")}
                  className={s.modal__inputs__names__last}
                />
              </div>
              <Input
                labelText="Username"
                value={accountInfo.username}
                setValue={(e) => updateAccountInfo(e, "username")}
              />
              <Input
                labelText="Password"
                value={accountInfo.password}
                setValue={(e) => updateAccountInfo(e, "password")}
                Password
              />
              <Input
                labelText="Confirm Password"
                value={accountInfo.confirmPassword}
                setValue={(e) => updateAccountInfo(e, "confirmPassword")}
                Password
              />
            </div>
            <div className={s.modal__buttons}>
              {confirmText !== undefined ? (
                <button
                  type="button"
                  className={s.modal__buttons__left}
                  onClick={confirmed}
                >
                  {confirmText}
                </button>
              ) : (
                ""
              )}
              {declineText !== undefined ? (
                <button
                  type="button"
                  className={s.modal__buttons__right}
                  onClick={declined}
                >
                  {declineText}
                </button>
              ) : (
                ""
              )}
            </div>
          </div>
        </>
      ) : (
        ""
      )}
    </div>
  );
};

Login.defaultProps = {
  open: false,
  confirmText: undefined,
  declineText: undefined,
  onConfirm: () => {},
  onDecline: () => {},
};

Login.propTypes = {
  open: PropTypes.bool,
  confirmText: PropTypes.string,
  declineText: PropTypes.string,
  onConfirm: PropTypes.func,
  onDecline: PropTypes.func,
};

export default Login;
