/* eslint-disable no-return-assign */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-static-element-interactions */

import React, { useEffect } from "react";
import { gsap } from "gsap";
import { ScrollTrigger } from "gsap/dist/ScrollTrigger";
import s from "./homepage.module.scss";
import TaskPreviewSmall from "../../../public/images/svg/TaskPreviewSmall.svg";
import TaskPreviewLarge from "../../../public/images/svg/TaskPreviewLarge.svg";
import GroceryPreviewSmall from "../../../public/images/svg/GroceryPreviewSmall.svg";
import GroceryPreviewLarge from "../../../public/images/svg/GroceryPreviewLarge.svg";
import ShopPreviewSmall from "../../../public/images/svg/ShopPreviewSmall.svg";
import ShopPreviewLarge from "../../../public/images/svg/ShopPreviewLarge.svg";

const HomePage = () => {
  const refs = [null, null, null];
  const messageRefs = [null, null, null];
  const previewRefs = [null, null, null];
  const updateActive = (e) => {
    refs.map((ref) => ref.classList.remove(s.active));
    e.target.classList.add(s.active);

    const myIndex = e.target.getAttribute("data-index");
    messageRefs.map((el) =>
      el.getAttribute("data-index") === myIndex
        ? el.classList.add(s.selected)
        : el.classList.remove(s.selected)
    );
    previewRefs.map((el) =>
      el.getAttribute("data-index") === myIndex
        ? el.classList.add(s.selected)
        : el.classList.remove(s.selected)
    );
  };

  let headerRef = null;
  let buttonRef = null;
  let imgRef = null;
  let listRef = null;
  useEffect(() => {
    gsap.registerPlugin(ScrollTrigger);

    const animateThis = () => {
      gsap.utils.toArray("[data-animate]").forEach((element) => {
        ScrollTrigger.create({
          trigger: element,
          start: "top 85%",
          onEnter: () => element.classList.add("animate"),
        });
      });
    };

    const staggerThis = () => {
      gsap.utils.toArray("[data-stagger]").forEach((element) => {
        const staggered = gsap.utils.toArray(element.children);

        gsap.set(staggered, { autoAlpha: 0, y: 40 });

        ScrollTrigger.create({
          trigger: element,
          start: "top 75%",
          onEnter: () =>
            gsap.to(staggered, {
              duration: 1,
              autoAlpha: 1,
              y: 0,
              stagger: 0.2,
              ease: "power2.out",
            }),
        });
      });
    };

    const timeL = gsap.timeline();
    timeL.set(headerRef, { autoAlpha: 0, y: 40 });
    timeL.set(buttonRef, { autoAlpha: 0, y: 40 });
    timeL.set(imgRef, { autoAlpha: 0, scale: 1.3 });
    gsap.utils.toArray(listRef).forEach((element) => {
      const listEls = gsap.utils.toArray(element.children);
      timeL.set(listEls, { autoAlpha: 0, y: 40 });
    });
    timeL.to(
      imgRef,
      {
        duration: 1,
        autoAlpha: 1,
        scale: 1,
        ease: "power2.out",
      },
      "+=0.5"
    );
    timeL.to(
      headerRef,
      {
        duration: 1,
        autoAlpha: 1,
        y: 0,
        ease: "power2.out",
      },
      "-=0.5"
    );
    timeL.to(
      buttonRef,
      {
        duration: 1,
        autoAlpha: 1,
        y: 0,
        ease: "power2.out",
      },
      "-=0.75"
    );
    gsap.utils.toArray(listRef).forEach((element) => {
      const listEls = gsap.utils.toArray(element.children);
      timeL.to(
        listEls,
        { duration: 1, autoAlpha: 1, y: 0, stagger: 0.15, ease: "power2.out" },
        "-=1.5"
      );
    });

    animateThis();
    staggerThis();
  }, []);

  return (
    <div className={s.root}>
      <div className={s.hero}>
        <div className={s.hero__left}>
          <h1 ref={(e) => (headerRef = e)}>Easy Chore & Grocery Managing</h1>
          <button type="button" ref={(e) => (buttonRef = e)}>
            Join Us
          </button>
        </div>
        <div className={s.hero__img} ref={(e) => (imgRef = e)}>
          <div className={s.hero__img__shadow}>
            <h1>Easy Chore & Grocery Managing</h1>
            <button type="button">Join Us</button>
          </div>
        </div>
      </div>
      <div className={s.list} ref={(e) => (listRef = e)}>
        <div
          className={`${s.list__item} ${s.active}`}
          onClick={(e) => updateActive(e)}
          ref={(e) => (refs[0] = e)}
          data-index="1"
        >
          <h3>Why would I use this?</h3>
          <div className={s.list__item__bar} />
        </div>
        <div
          className={s.list__item}
          onClick={(e) => updateActive(e)}
          ref={(e) => (refs[1] = e)}
          data-index="2"
        >
          <h3>What about Groceries?</h3>
          <div className={s.list__item__bar} />
        </div>
        <div
          className={s.list__item}
          onClick={(e) => updateActive(e)}
          ref={(e) => (refs[2] = e)}
          data-index="3"
        >
          <h3>What are Points for?</h3>
          <div className={s.list__item__bar} />
        </div>
      </div>
      <div className={s.preview} data-animate="fade-in-up">
        <div
          className={s.preview__bar}
          data-animate="grow-across"
          data-origin="left"
        />
        <div className={s.preview__message}>
          <div
            className={`${s.preview__message__box} ${s.selected}`}
            data-index="1"
            ref={(e) => (messageRefs[0] = e)}
          >
            <h2>Why would I use this?</h2>
            <p>
              Imagen you get home from work, and your house is filthy, none of
              your kids cleaned anything, because they didn’t know to. With
              TaskMaster, your kids can hop on from their computer, or their
              phone, and simply look at today’s tasks, which you made, and start
              doing them.
            </p>
          </div>
          <div
            className={s.preview__message__box}
            data-index="2"
            ref={(e) => (messageRefs[1] = e)}
          >
            <h2>What about Groceries?</h2>
            <p>
              Yes, you can also have a grocery list! It’s easy to add, delete,
              and edit items. You can just look at your phone anytime while you
              are shopping, and every item you need will be right there.
            </p>
          </div>
          <div
            className={s.preview__message__box}
            data-index="3"
            ref={(e) => (messageRefs[2] = e)}
          >
            <h2>What are Points for?</h2>
            <p>
              Points are used to reward your child, for completing a task. They
              can use these points to buy stuff from the shop, like having
              friends over or video time. And the best part, you can to create
              all of these yourself, and kind of reward you want.
            </p>
          </div>
        </div>
        <div className={s.preview__box}>
          <div
            className={`${s.preview__box__svg} ${s.selected}`}
            data-index="1"
            ref={(e) => (previewRefs[0] = e)}
          >
            <TaskPreviewSmall />
          </div>
          <div
            className={s.preview__box__svg}
            data-index="2"
            ref={(e) => (previewRefs[1] = e)}
          >
            <GroceryPreviewSmall />
          </div>
          <div
            className={s.preview__box__svg}
            data-index="3"
            ref={(e) => (previewRefs[2] = e)}
          >
            <ShopPreviewSmall />
          </div>
        </div>
      </div>
      <div className={s.values}>
        <div className={s.values__value}>
          <div className={s.values__value__left}>
            <div
              className={s.values__value__left__bar}
              data-animate="grow-across"
              data-origin="left"
            />
            <h2 data-animate="fade-in-up">Why would I use this?</h2>
            <p data-animate="fade-in-up">
              Imagen you get home from work, and your house is filthy, none of
              your kids cleaned anything, because they didn’t know to. With
              TaskMaster, your kids can hop on from their computer, or their
              phone, and simply look at today’s tasks, which you made, and start
              doing them.
            </p>
          </div>
          <div className={s.values__value__preview} data-animate="fade-in-up">
            <TaskPreviewLarge />
          </div>
        </div>
        <div className={s.values__value2}>
          <div className={s.values__value2__left}>
            <div
              className={s.values__value2__left__bar}
              data-animate="grow-across"
              data-origin="right"
            />
            <h2 data-animate="fade-in-up">What about Groceries?</h2>
            <p data-animate="fade-in-up">
              Yes, you can also have a grocery list! It’s easy to add, delete,
              and edit items. You can just look at your phone anytime while you
              are shopping, and every item you need will be right there.
            </p>
          </div>
          <div className={s.values__value2__preview} data-animate="fade-in-up">
            <GroceryPreviewLarge />
          </div>
        </div>
        <div className={s.values__value}>
          <div className={s.values__value__left}>
            <div
              className={s.values__value__left__bar}
              data-animate="grow-across"
              data-origin="left"
            />
            <h2 data-animate="fade-in-up">What are Points for?</h2>
            <p data-animate="fade-in-up">
              Points are used to reward your child, for completing a task. They
              can use these points to buy stuff from the shop, like having
              friends over or video time. And the best part, you can to create
              all of these yourself, and kind of reward you want.
            </p>
          </div>
          <div className={s.values__value__preview} data-animate="fade-in-up">
            <ShopPreviewLarge />
          </div>
        </div>
      </div>
      <div className={s.footer}>
        <h1>TaskMaster</h1>
      </div>
    </div>
  );
};

export default HomePage;
