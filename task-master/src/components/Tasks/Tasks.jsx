import React, { useState } from "react";
import s from "./tasks.module.scss";
import Login from "../Login/Login";
import data from "./data";
import TaskPaper from "./components/TaskPaper/TaskPaper";

const Tasks = () => {
  const [tasks] = useState(data);

  const confirmedSignUp = () => {
    console.log("You have been signed up!");
  };

  return (
    <>
      <div className={s.root}>
        <div className={s.tasksHolder}>
          <div>
            <h2 className={s.tasksHolder__header}>Tasks</h2>
          </div>
          <div className={s.tasksHolder__tasksWrapper}>
            {tasks.map((task) => (
              <TaskPaper
                name={task.assignee}
                title={task.title}
                tasks={task.tasks}
                id={task.id}
              />
            ))}
          </div>
        </div>
      </div>
      <Login
        open
        confirmText="Sign Up"
        declineText="Cancel"
        onConfirm={confirmedSignUp}
      />
    </>
  );
};

export default Tasks;
