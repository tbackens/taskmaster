import React from "react";
import PropTypes from "prop-types";
import Link from "next/link";
import s from "./taskpaper.module.scss";

const TaskPaper = ({ name, title, tasks, id }) => {
  const initials = (nameString) =>
    nameString.split(" ")[0][0] + nameString.split(" ")[1][0];

  return (
    <Link href="tasks/[id]" as={`/tasks/${id}`}>
      <div className={s.root}>
        <div className={s.paper}>
          <div className={s.paper__top}>
            <div className={s.paper__top__user}>
              <div className={s.paper__top__user__avatar}>{initials(name)}</div>
              <h2 className={s.paper__top__user__name}>{name}</h2>
            </div>
            <div className={s.paper__top__titleWrapper}>
              <h1 className={s.paper__top__titleWrapper__title}>{title}</h1>
            </div>
          </div>
          <div className={s.paper__bottom}>
            <ul className={s.paper__bottom__tasksList}>
              {tasks.map((task) => (
                <li className={s.paper__bottom__tasksList__task}>
                  <div className={s.paper__bottom__tasksList__task__box}>
                    <p>{task.title}</p>
                  </div>
                </li>
              ))}
            </ul>
          </div>
          <div className={s.paper__shadow} />
        </div>
      </div>
    </Link>
  );
};

TaskPaper.defaultProps = {
  id: undefined,
};

TaskPaper.propTypes = {
  name: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  tasks: PropTypes.array.isRequired,
  id: PropTypes.number,
};

export default TaskPaper;
