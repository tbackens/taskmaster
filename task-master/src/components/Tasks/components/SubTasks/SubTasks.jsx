import React, { useState } from "react";
import PropTypes from "prop-types";
import TaskWrapper from "../../../TaskWrapper/TaskWrapper";
import s from "./subTasks.module.scss";

const SubTasks = ({ id, data }) => {
  const [showMyTasks, setShowMyTasks] = useState(false);

  const correctTask = data.filter((obj) => obj.id === id);

  const handleMyTasksChange = () => setShowMyTasks(!showMyTasks);

  return (
    <div className={s.root}>
      <div className={s.mainPaper}>
        <div className={s.mainPaper__top}>
          <div className={s.mainPaper__top__left}>
            <h3 className={s.mainPaper__top__left__participantsHeader}>
              Participants
            </h3>
            <div className={s.mainPaper__top__left__participants}>
              <p>{correctTask[0].participants.join(" - ")}</p>
            </div>
          </div>
          <div className={s.mainPaper__top__right}>
            <button
              type="button"
              className={s.mainPaper__top__right__myTasksButton}
              onClick={handleMyTasksChange}
            >
              {showMyTasks ? "All Tasks" : "My Tasks"}
            </button>
          </div>
        </div>
        <div className={s.mainPaper__bottom}>
          {showMyTasks ? (
            <TaskWrapper
              id="Thomas Backens"
              data={correctTask}
              taskAssignee={correctTask[0].assignee}
              taskTitle={correctTask[0].title}
            />
          ) : (
            <TaskWrapper id={id} data={data} taskTitle={correctTask[0].title} />
          )}
        </div>
      </div>
    </div>
  );
};

SubTasks.propTypes = {
  id: PropTypes.string.isRequired,
  data: PropTypes.array.isRequired,
};

export default SubTasks;
