import React from "react";
import PropTypes from "prop-types";
import s from "./card.module.scss";

const Card = ({ title, avatar, description, rightImage, color }) => {
  return (
    <div className={s.root}>
      <div className={s.main} style={{ backgroundColor: color }}>
        {avatar ? (
          <>
            <div className={s.main__cardTop}>
              <div className={s.main__cardTop__cardTitle} />
              <div className={s.main__cardTop__cardTopRight}>
                <div
                  style={{
                    paddingRight: 10,
                    color: color === "white" ? "black" : "white",
                  }}
                >
                  {rightImage}
                </div>
              </div>
            </div>
            <div className={s.main__username}>
              <div className={s.main__username__avatar}>TB</div>
              <h3 className={s.cardTitle}>{title}</h3>
            </div>
          </>
        ) : (
          <>
            <div className={s.main__cardTop}>
              <h4
                className={s.main__cardTop__cardTitle}
                style={{ color: color === "white" ? "black" : "white" }}
              >
                {title}
              </h4>
              <div className={s.main__cardTop__cardTopRight}>
                <div
                  style={{
                    paddingRight: 10,
                    color: color === "white" ? "black" : "white",
                  }}
                >
                  {rightImage}
                </div>
              </div>
            </div>
            <div className={s.main__cardBottom}>
              <p
                className={s.main__cardBottom__cardDescription}
                style={{ color: color === "white" ? "black" : "white" }}
              >
                {description}
              </p>
            </div>
          </>
        )}
      </div>
    </div>
  );
};

Card.defaultProps = {
  description: "",
  avatar: null,
  rightImage: null,
  color: "white",
};

Card.propTypes = {
  title: PropTypes.string.isRequired,
  avatar: PropTypes.element,
  description: PropTypes.string,
  rightImage: PropTypes.element,
  color: PropTypes.string,
};

export default Card;
