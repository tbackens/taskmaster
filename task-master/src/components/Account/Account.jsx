/* eslint-disable react/jsx-wrap-multilines */

import React from "react";
import MoreHorizIcon from "@material-ui/icons/MoreHoriz";
import Card from "./Card/Card";
import TaskWrapper from "../TaskWrapper/TaskWrapper";
import data from "../Tasks/data";
import s from "./account.module.scss";

const Account = () => {
  return (
    <div className={s.root}>
      <div className={s.accountInfo}>
        <div className={s.accountInfo__card}>
          <Card
            title="My Items"
            description="This is the amount of items you have."
            rightImage={<p style={{ textAlign: "right" }}>4 Items</p>}
            color="#003fa3"
          />
        </div>
        <div className={s.accountInfo__card}>
          <Card
            title="Thomas Backens"
            avatar
            rightImage={
              <button type="button" className={s.accountInfo__card__button}>
                <MoreHorizIcon style={{ fill: "grey" }} />
              </button>
            }
          />
        </div>
        <div className={s.accountInfo__card}>
          <Card
            title="My Points"
            description="Points are awarded to you for completing tasks."
            rightImage={<p style={{ textAlign: "right" }}>1,627</p>}
            color="#003fa3"
          />
        </div>
      </div>
      <div className={s.taskPaper}>
        {data
          .map((obj) => obj.assignee)
          .filter((val, id, array) => array.indexOf(val) === id)
          .map((obj) => (
            <TaskWrapper
              id="Thomas Backens"
              data={data}
              taskAssignee={obj}
              key={obj}
              taskTitle="My Tasks"
            />
          ))}
      </div>
    </div>
  );
};

export default Account;
