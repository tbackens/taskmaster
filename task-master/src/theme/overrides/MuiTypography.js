export default {
  gutterBottom: {
    marginBottom: 8,
  },
  h6: {
    fontSize: 18,
    letterSpacing: 0.15,
  },
};
