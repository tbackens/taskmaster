import React from "react";
import data from "../../components/Tasks/data";
import SubTasks from "../../components/Tasks/components/SubTasks/SubTasks";

const Task = ({ id }) => {
  return <SubTasks id={id} data={data} />;
};

Task.getInitialProps = async ({ query }) => {
  const { id } = query;
  return { id };
};

export default Task;
